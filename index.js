/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/	
	
	//first function here:

	function printBasicInformation(){
		let fullName =prompt("Enter your Full Name:");
		let age=prompt("Enter your Age:");
		let location=prompt("Enter your Location:");

		console.log("Hello, "+fullName);
		console.log("You are "+age+" years old.");
		console.log("You live in " +location);
	}
	printBasicInformation();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//second function here:

	function printFavoriteBands(){
		let beatles="1. The Beatles";
		let metallica="2. Metallica";
		let eagles="3. The Eagles";
		let ciel="4. L'arc~en~Ciel";
		let eHeads="5. Eraserheads";
		console.log(beatles);
		console.log(metallica);
		console.log(eagles);
		console.log(ciel);
		console.log(eHeads);
	}
	printFavoriteBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	let printFavoriteMovies = function printMovieTitle(){
	
	let movie1 = ("1. The Godfather"); 
	let rating1 = ("Rotten Tomatoes Rating:97%"); 
	let movie2 = ("2. The Godfather, Part II"); 
	let rating2 = ("Rotten Tomatoes Rating:96%"); 
	let movie3 = ("3. Shawshank Redemption");
	let rating3 = ("Rotten Tomatoes Rating:91%"); 
	let movie4 = ("4. To Kill A Mockingbird");
	let rating4 = ("Rotten Tomatoes Rating:93%"); 
	let movie5 = ("5. Psycho");
	let rating5 = ("Rotten Tomatoes Rating:96%"); 

	
	console.log(movie1);
	console.log(rating1);
	console.log(movie2); 
	console.log(rating2);
	console.log(movie3); 
	console.log(rating3);
	console.log(movie4); 
	console.log(rating4);
	console.log(movie5); 
	console.log(rating5);
};
printFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


